
 <?php 
require("/var/www/superstats.dk/genbrug/utils.php");

if (!isset($page_title)) {
	$page_title = "SuperStats - Superligaen i tal";
}
?>

<!DOCTYPE html>

<html>

<?php
$today = date("d.m.Y H:i:s");
echo "<!-- Generated: " . $today . " -->\n";




?>


<head>

<script data-ad-client="ca-pub-1897583251110681" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>


<?php
// hack for forside
$page_url = $_SERVER["REQUEST_URI"];
if ($page_url == "/index.php") {
	$page_url = "/";
}

echo "<link rel=\"canonical\" href=\"https://superstats.dk" . $page_url . "\" />\n";

if (isset($page_meta)) {
	echo "<meta name=\"description\" content=\"" . $page_meta . "\" />";
}

echo "<title>" . $page_title . "</title>";


?>

<meta charset="utf-8">
<meta name="Keywords" content="fodbold,bold,sport,resultater,superliga,liga,em,vm,cup,pokal,pokalturnering,dbu pokal, dbu pokalen,sas,danmark,aab,ob,brøndby,brøndby if,bif,fc midtjylland,fcm,randers fc,randers,rfc,ac horsens,horsens,ach,esbjerg fb,esbjerg,efb,fc nordsjælland,fcn,sønderjyske,agf,odense,ob,fc københavn,fck,vejle,vff,silkeborg if,sif,hobro ik,hik,fodbold resultater,fodbold tabel,fodbold tabeller" />
<meta name="Robots" content="all" />
<meta name="Revisit-after" content="2" />
<meta name="Author" content="superstats.dk" />
<meta name="Copyright" content="superstats.dk, 2007-22. All rights reserved" />
<meta http-equiv="Content-language" content="dan" />
<meta name="Rating" content="General" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-store" />

	<link rel="shortcut icon" href="/favicon.ico">
	
	<link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="/stil.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="simulate.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="/css/sorTable.css" media="screen" />
  
	
	<script type="text/javascript" src="/js/jquery.min.js"></script>
	<script type="text/javascript" src="/js/base4.js"></script>
	<script type="text/javascript" src="/js/jquery.metadata.min.js"></script>
	<script type="text/javascript" src="/js/jquery.bgiframe.min.js"></script>
  <script type="text/javascript" src="/js/sort-table.js"></script>
	<script type="text/javascript" src="/js/jquery.tablesorter.min.js"></script>
	<script type="text/javascript" src="/js/jquery.tablesorter.js"></script>
	<script type="text/javascript" src="/js/MatchProbability.js"></script>
	<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="simulate.js"></script>
  <script type="text/javascript" src="https://www.google.com/jsapi"></script>
  <script type="text/javascript">google.load('visualization', '1.0', {'packages':['corechart']});</script>


<script>
$(document).ready(function(){
	$("#search-box").keyup(function(){
		$.ajax({
		type: "POST",
		url: "/sog/readPlayer.php",
		data:'keyword='+$(this).val(),
		beforeSend: function(){
			$("#search-box").css("background","#FFF url(/images/LoaderIcon.gif) no-repeat 165px");
		},
		success: function(data){
			$("#suggesstion-box").show();
			$("#suggesstion-box").html(data);
			$("#search-box").css("background","#FFF");
		}
		});
	});
});

function selectPlayer(val) {
$("#search-box").val(val);
$("#suggesstion-box").hide();
}
</script>

</head>

<body>

<?php include_once($_SERVER["DOCUMENT_ROOT"]."/genbrug/analyticstracking.php") ?>

<div id="outer">


<div id="banner" style="text-align:center; margin:10px auto;">
<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- top -->
<ins class="adsbygoogle"
     style="display:inline-block;width:728px;height:90px"
     data-ad-client="ca-pub-1897583251110681"
     data-ad-slot="9488282179"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>

</div>



  <div id="wrap">

   <div id="header">

  <h4 id="logo"><a href="/">SuperStats</a></h4>
      <ul id="nav">
        <li><span><a class="menu" onclick="void(0)"><span>Hold</span></a>
          <ul>
			<li><a href="/hold/sason?id=13" class="menu">AGF</a></li>
			<li><a href="/hold/sason?id=2" class="menu">Brøndby IF</a></li>
			<li><a href="/hold/sason?id=4" class="menu">FC København</a></li>
			<li><a href="/hold/sason?id=5" class="menu">FC Midtjylland</a></li>
			<li><a href="/hold/sason?id=6" class="menu">FC Nordsjælland</a></li>
			<li><a href="/hold/sason?id=7" class="menu">OB</a></li>
			<li><a href="/hold/sason?id=8" class="menu">Randers FC</a></li>
 			<li><a href="/hold/sason?id=9" class="menu">Silkeborg IF</a></li>
			<li><a href="/hold/sason?id=26" class="menu">SønderjyskE</a></li>
			<li><a href="/hold/sason?id=10" class="menu">Vejle BK</a>
 			<li><a href="/hold/sason?id=11" class="menu">Viborg FF</a></li>
			<li><a href="/hold/sason?id=12" class="menu">AaB</a></li>
          </ul>
        </li>
        <li><a class="menu" onclick="void(0)"><span>Kampe</span></a>
          <ul>
			<li><a href="/stilling/" class="menu">Stillingen</a></li>
			<li><a href="/program" class="menu">Kampprogram</a></li>
			<li><a href="/tilskuere/" class="menu">Tilskuere</a></li>
			<li><a href="/streak/" class="menu">Serier</a></li>
          </ul>
        </li>
        <li><a class="menu" onclick="void(0)"><span>Spillere</span></a>
          <ul>
			<li><a href="/spillere/spilletid" class="menu">Spilletid</a></li>
			<li><a href="/spillere/udl-klub" class="menu">Udlændinge</a></li>
			<li><a href="/spillere/alder" class="menu">Yngst / ældst</a></li>
			<li><a href="/spillere/trans/" class="menu">Klub-skifter</a></li>
			<li><a href="/spillere/kontrakt-udlob" class="menu">Kontrakter</a></li>
          </ul>
        </li>
        <li><a class="menu" onclick="void(0)"><span>Mål</span></a>
          <ul>
			<li><a href="/maal/" class="menu">Topscorere</a></li>
			<li><a href="/maal/assist" class="menu">Assist</a></li>
			<li><a href="/maal/plus-assist" class="menu">Mål + assist</a></li>
			<li><a href="/maal/straffespark/" class="menu">Straffespark</a></li>
			<li><a href="/maal/selvmaal" class="menu">Selvmål</a></li>
			<li><a href="/maal/minutter" class="menu">Minutter</a></li>
			<li><a href="/maal/runde" class="menu">Runder</a></li>
			<li><a href="/maal/over-under" class="menu">Over/Under</a></li>
          </ul>
        </li>
        <li><a class="menu" onclick="void(0)"><span>Kort</span></a>
          <ul>/
			<li><a href="/kort/" class="menu">Kort</a></li>
			<li><a href="/kort/dommere/dommere" class="menu">Dommere</a></li>
			<li><a href="/kort/rode" class="menu">Udvisninger</a></li>
			<li><a href="/kort/karantaner" class="menu">Karantæner</a></li>
			<li><a href="/kort/fare" class="menu">I karantæne-fare</a></li>
          </ul>
        </li>
        <li><a class="menu" onclick="void(0)"><span>Andre</span></a>
           <ul>
			<li><a href="/1div/" class="menu">1. division</a></li>
			<li><a href="/pokal/" class="menu">Pokal</a></li>
			<li><a href="/europa/" class="menu">Europa</a></li>
			<li><a href="/vm/" class="menu">VM</a></li>
			<li><a href="/em/" class="menu">EM</a></li>
          </ul>
        </li>
        <li class="sitemaap"><a href="/sitemap" class="menu"><span>Sitemap</span></a>
         </li>

      </ul>

    </div>







    <div id="ikoner">
	  <a href="/hold/alltime?id=13" class="agf" title="AGF"></a>
      <a href="/hold/alltime?id=2" class="brondby" title="Brøndby IF"></a>
      <a href="/hold/alltime?id=4" class="fck" title="FC København"></a>
      <a href="/hold/alltime?id=5" class="fcm" title="FC Midtjylland"></a>
      <a href="/hold/alltime?id=6" class="fcn" title="FC Nordsjælland"></a>
      <a href="/hold/alltime?id=7" class="ob" title="OB"></a>
      <a href="/hold/alltime?id=8" class="randersfc" title="Randers FC"></a>
      <a href="/hold/alltime?id=9" class="silkeborgif" title="Silkeborg IF"></a>
      <a href="/hold/alltime?id=26" class="sonderjyske" title="SønderjyskE"></a>
      <a href="/hold/alltime?id=10" class="vejle" title="Vejle BK"></a>
      <a href="/hold/alltime?id=11" class="viborgff" title="Viborg FF"></a>
      <a href="/hold/alltime?id=12" class="aab" title="AaB"></a>

	  <a href="https://www.twitter.com/superstats_dk" class="twitter" title="SuperStats på Twitter" /></a>
	  <a href="https://www.facebook.com/superstats.dk" class="facebook" title="SuperStats på Facebook"></a>
	  <a href="/sog/" class="find" title="Søg på SuperStats" /></a>

</div>

    
    <div id="content">


<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/da_DK/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>





	  
  <div class="site-banner-left">
            <div id="leftAds">
<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- sides -->
<ins class="adsbygoogle"
     style="display:inline-block;width:160px;height:600px"
     data-ad-client="ca-pub-1897583251110681"
     data-ad-slot="4898300475"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>
            </div>	
          </div>
          <div class="site-banner-right">
            <div id="rightAds">
<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- rightads -->
<ins class="adsbygoogle"
     style="display:inline-block;width:160px;height:600px"
     data-ad-client="ca-pub-1897583251110681"
     data-ad-slot="5290603593"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>
            </div>
          </div>
		  


