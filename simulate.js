verbose=true
// Global variables
if (typeof(nSimulations) == "undefined") 
  nSimulations=50000
MatchProb=new MatchProbability()
simMatches=null
DEBUG=0
timeStart=0

$(document).ready(function(){
  // When "Run" button is pressed, start the simulation
  $("#table_result button").click(runSimulation)
  var sliders=$("#table_current input")
  sliders.on("change",function(e) {
    slidestop()
  })
  sliders.bind("input",function(e) {
    updateSkill(this)
    updateAutoOdds(this.getAttribute("data-tid"));
  })
  $("#table_matchlist .spanodds > input").on("change", updateTitleChange)
  document.getElementById("str_reset").onclick=function(){setSkillAll(false, 6, -2.2)}
  document.getElementById("str_equal").onclick=function(){setSkillAll(true)}
  document.getElementById("str_reset").click()
  $(".outcome").click(selectOutcome)
  $(".oddsreset").click(oddsReset)
  $(".restype").click(resType)

  $("#progressbar").progressbar({value: 0});

  for (x in Teams) {
    Teams[x].results=null
    Teams[x].rescomp=null
  }
  for (mid in Matches) {
    Matches[mid].restype="odds"
    if (Matches[mid].hjemmemaal!=null) {
      resType($("#match_"+mid+" .restype")[0])
    }
  }

  $("#table_current .help").click(help_skill)
  $("#help_result").click(help_result)
  $("#help_run").click(help_run)
  $("#table_matchlist .help").click(help_match)

  //$("#helpdialog").click(function (e) { e.stopPropagation(); });
  //$(document).click(function () { $("#helpdialog").dialog('close') });
})

function setSkillAll(equal, a=2.4, b=2) {
  var sliders=$("#table_current input")
  sliders.each(function(i,s) {setSkill(s,equal,a,b)})
  updateAutoOdds()
}

function setSkill(slider, equal=false, a, b) {
  var tid=slider.getAttribute("data-tid")
  var val;
  if (equal) val=5
  else val=(b+a*Teams[tid].ps/Teams[tid].gs).fix1()
  slider.value=val
  updateSkill(slider)
}

function updateSkill(e) {
  var skill=parseFloat(e.value)
  var tid=e.getAttribute("data-tid")
  Teams[tid].skill=skill
  e.nextElementSibling.textContent=skill.toFixed(1)
}

function slidestop(e) {
  setTimeout(function() {
    $("#table_matchlist span.spanodds input").removeClass("changed")
  }, 800);
}

function updateAutoOdds(tid=null) {
  for (var mid in Matches) {
    match=Matches[mid]
    if (tid!=null && tid!=match.hjemme && tid!=match.ude) continue
    match.autoodds=MatchProb.MatchOdds(Teams[match.hjemme].skill-Teams[match.ude].skill);
    writeMatchOdds(match, (tid>0))
  }
}

function writeMatchOdds(match, color=false) {
  odds=odds2input(match.autoodds)
  inputs=$("#match_"+match.kampid+" .spanodds > input")
  inputs.each(function(i,s) {s.value=odds[i]})
  if (color) inputs.addClass("changed")
  updateOddsTitle(inputs[0].parentElement, odds)
}

function updateTitleChange(e) {
  var span=e.target.parentElement
  var odds=Array.from(span.children).map(x => parseFloat(x.value))
  updateOddsTitle(span,odds)
}

function updateOddsTitle(span, odds) {
  span.setAttribute("title",probs2odds(odds))
}

function selectOutcome(e) {
  var inputs=e.target.parentElement.previousSibling.children[0].children
  if      (inputs[0].value=="1") odds=[0,1,0]
  else if (inputs[1].value=="1") odds=[0,0,1]
  else                           odds=[1,0,0]
  inputs[0].value=odds[0]
  inputs[1].value=odds[1]
  inputs[2].value=odds[2]
}

function oddsReset(e) {
  var tr=e.target.parentElement.parentElement
  var mid=tr.id.substr(6)
  writeMatchOdds(Matches[mid])
  if (Matches[mid].hjemmemaal!=null) {
    inputs=$("#match_"+match.kampid+" .spanresult > input")
    inputs[0].value = Matches[mid].hjemmemaal
    inputs[1].value = Matches[mid].udemaal
    if(Matches[mid].restype!="res") e.target.parentElement.previousSibling.click()
  }
  else {
    if(Matches[mid].restype=="res") e.target.parentElement.previousSibling.click()
  }
}

function res2odds(res) {
  if (res.trim()=="1") return([1,0,0])
  if (res.trim()=="x") return([0,1,0])
  if (res.trim()=="2") return([0,0,1])
  throw "Unknown result"
}

function resType(e) {
  var td
  // I want to be able to call this function both as a click target and just with the element as arg
  if (e.target) td=e.target
  else          td=e
  if (td.tagName!="TD") td=td.parentElement
  var tdOutcome=td.previousSibling
  var tdOdds=tdOutcome.previousSibling
  var mid=td.parentElement.id.substr(6)
  var spanodds=tdOdds.children[0]
  var spanres=tdOdds.children[1]
  if (Matches[mid].restype.substr(0,4)=="odds") {
    Matches[mid].restype="res"
    spanodds.style.display="none"
    spanres.style.display="inline"
    Array.from(td.children).forEach(function(i) {classToggle(i, "typeselected", "typeunselected")})
    tdOutcome.children[0].style.visibility="hidden"
  } else {
    Matches[mid].restype="odds"
    //tdOutcome.style.display="none"
    spanodds.style.display="inline"
    spanres.style.display="none"
    Array.from(td.children).forEach(function(i) {classToggle(i, "typeselected", "typeunselected")})
    tdOutcome.children[0].style.visibility="visible"
  }
}

function classToggle(e, c1, c2) {
  if (e.classList.value==c1) {
    e.classList.remove(c1)
    e.classList.add(c2)
  } else {
    e.classList.remove(c2)
    e.classList.add(c1)
  }
}

function debug(msg) {
  if (verbose) console.log(msg)
}

Number.prototype.fix1 = function(f){
  return(Math.round(this*10)/10)
}

function odds2input(odds) {
  var p1=Math.round(odds[0]*100)
  var p2=Math.round(odds[2]*100)
  var px=100-p1-p2
  return([p1,px,p2])
}

function input2odds(inputs) {
  var p1=parseFloat(inputs[0])
  var px=parseFloat(inputs[1])
  var p2=parseFloat(inputs[2])
  var sum=p1+px+p2
  // Check that sum is greater than 0 and that no odds are negative
  if (!(sum>0 && p1>=0 && px>=0 && p2>=0)) return(false)
  return([p1/sum, px/sum, p2/sum])
}

function probs2odds(probs) {
  var sum=probs[0]+probs[1]+probs[2]
  var out=probs.map(function(x){ if (x==0) return("∞"); else return((sum/x).toFixed(2))})
  return("Svarer til odds: "+out[0]+" / "+out[1]+" / "+out[2])
}

function isAutoOdds(userodds, autoodds) {
  var cmpodds=input2odds(odds2input(autoodds))
  for (var i=0;i<3;i++)
    if (cmpodds[i]!=userodds[i]) return(false)
  return(true)
}

function checkValidInput() {
  return(true)
}

function runSimulation() {
  debug("Running")
  if (!checkValidInput()) return;
  // Read odds from input fields - if false then just return due to odds error
  if (!readUserOdds()) return;
  prepareRun()
  // Disable run button
  // $("#table_result button").html("Vent...").prop('disabled', true);
  timeStart=new Date();
  // Start the loop function, with total simulations and how many allready done: 0
  loopSim(nSimulations,0);
}

function readUserOdds() {
  var oddsError="";
  $("#table_matchlist tbody tr").each(function(i,row) {
    var mid=row.id.substr(6)
    var match=Matches[mid];             // Get match info for this match
    var inputs=$(row).find("input");    // Get all inputs from this matchrow
    if (match.restype.substr(0,4)=="odds") {
      match.restype="odds"
      var inputvals=[inputs.eq(0).val(), inputs.eq(1).val(), inputs.eq(2).val()]
      var runodds=input2odds(inputvals)
      if (!runodds) oddsError+="Invalid odds for match "+i+"\n"
      match.runodds=runodds
    } else if (match.restype=="res") {
      var res=[
        parseInt(inputs.eq(3).val()),
        parseInt(inputs.eq(4).val())
      ]
      if (isNaN(res[0]) || isNaN(res[1])) oddsError+="Invalid result for match "+i+"\n"
      match.runres=res
    } else
      debug("Unknown restype for match "+match.kampid+": "+match.restype)
  })
  if (oddsError) {alert(oddsError); return(false);}
  else return(true);
}

/* Loop through all matches, and add fixed results to base result table.
 * We don't need to iterate through matches with a fixed result. Those can
 * just be added once and for all */ 
function prepareRun() {
  for (var tid in Teams) {
    team=Teams[tid]
    team.rescomp=team.results;
    team.results=resObject()
    team.baseres={id:tid, g:team.g0, p:team.p0, d:team.gf0-team.ga0}
  }
  simMatches=[]
  for (var m in Matches) {
    match=Matches[m]
    if (match.restype.substr(0,4)=="odds") {
      if (isAutoOdds(match.runodds, match.autoodds)) {
        match.restype="oddsauto"
      }
      simMatches.push(match)
    }
    else {
      play(Teams[match.hjemme].baseres, match.runres[0]-match.runres[1])
      play(Teams[match.ude].baseres, match.runres[1]-match.runres[0])
    }
  }
}

function resObject() {
  return({avgpoint:0, position:Array(12).fill(0)})
}

function play(table, goaldif) {
  table.g++
  if      (goaldif>0) table.p+=3
  else if (goaldif==0) table.p+=1
  table.d+=goaldif
}

function loopSim(nSimulations, currentSim) {
  var simBlock=Math.min(nSimulations-currentSim,Math.max(500,Math.round(nSimulations/100)));
  for (var nSim=0;nSim<simBlock;nSim++) {
    result=runOnce();     // Run all matches a single time
  }

  var partCompleted=(currentSim+simBlock)/nSimulations;
  var timeEllapsed=new Date()-timeStart;
  var timeLeft=timeEllapsed*(nSimulations/(currentSim+simBlock)-1);

  // Update the progress bar
  // Textfield for percentage completed
  var progress="Fuldført: "+Math.round(100*partCompleted)+"%";
  // If we are done, replace text with "done"
  if (currentSim+simBlock>=nSimulations) progress="Færdig ("+num1000(nSimulations)+" simulationer)";
  $("#progressbar").progressbar("value",Math.round(100*partCompleted))
                   .children('.ui-progressbar-value')
  $("#progresslabel").html(progress);
  // If we have NOT reached the desired number of simulations, schedule some more
  if (currentSim+simBlock<nSimulations) {
    setTimeout(function() {loopSim(nSimulations,currentSim+simBlock);},3);
    return;
  }

  // OK we are finished. We have reached nSimulations.
  // =================================================
  calcResult()
  // Update results table
  showResult()
}

function runOnce() {
  // Reset table
  var table={};
  var stable=[]
  for (var tid in Teams) {
    table[tid]={ ... Teams[tid].baseres }  // spread syntax to ensure new copy and not a ref
    stable.push(table[tid])
  }
  //dumptable(table)
  simMatches.forEach(function(m) {
    if (m.restype=="oddsauto") {
      res=MatchProb.MatchSkillScore(Teams[m.hjemme].skill-Teams[m.ude].skill)
    }
    else if (m.restype=="odds") {
      res=getResult(MatchProb.MatchResultOdds(m.runodds))
    }
    else debug("UNKNWON TYPE")

    //dumpMatch(m, res)

    play(table[m.hjemme], res[0]-res[1])
    play(table[m.ude],    res[1]-res[0])
  })
  stable.sort(sortTable)
  stable.forEach(function(t,pos) {
    Teams[t.id].results.avgpoint+=t.p
    // Teams[t.id].results.avgpos+=pos+1
    // if (pos<=5) Teams[t.id].results.slutspil++
    Teams[t.id].results.position[pos]++
  })
}

function calcResult() {
  // Divide all totalt with number of simulations, to get correct avg/percentage
  for (tid in Teams) {
    var res=Teams[tid].results;
    res.avgpoint/=nSimulations
    res.avgpos=0
    res.position=res.position.map(i => i/nSimulations)
    if (simPart=="slutspil") {
      res.slutspil   = sumpos(res,1,6)
    } else if (simPart=="nedrykning") {
      res.playoff    = sumpos(res,1)   // 1'er i kvalspil
      res.pos_8_10   = sumpos(res,2,4)
      res.nedrykning = sumpos(res,5,6)
    } else if (simPart=="medaljer") {
      res.gold       = sumpos(res,1)   // 1'er i slutspil
      res.silver     = sumpos(res,2) 
      res.bronze     = sumpos(res,3) 
      res.pos_4_6    = sumpos(res,4,6)
    }
  }
}

function percent(value, always_show_sign=false) {
  return((value>0 && always_show_sign?"+":"")+(100*value).toFixed(1)+"%")
}

function sumpos(res, from, to=null) {
  var sum=arraysum(res.position.slice(from-1, (to?to:from)))
  return(sum);
}

  // Update simulation counts
  //$.ajax({url: "/group_stage_sim/simulationcount.php?simulations="+nSimulations, success: function(result){
  //  $("#simcount").html(result);
  //}});

function showResult() {
  for (t in Teams) {
    var tds=$("#res_"+t+" td")
    var res=Teams[t].results
    var old=Teams[t].rescomp
    tds.eq(3).html(res.avgpoint.toFixed(1))
    if (simPart=="slutspil") {
      tds.eq(4).html(res.avgpos.toFixed(1))
      tds.eq(5).html(res.slutspil)
      if (old) tds.eq(6).html(nt(res.slutspil-old.slutspil), true)
    } else if (simPart=="medaljer") {
      tds.eq(4).html(percent(res.gold))
      tds.eq(5).html(percent(res.silver))
      tds.eq(6).html(percent(res.bronze))
      tds.eq(7).html(percent(res.pos_4_6))
      if (old) {
        tds.eq(4).attr("title","Ændring: "+percent(res.gold-old.gold, true))
        tds.eq(5).attr("title","Ændring: "+percent(res.silver-old.silver, true))
        tds.eq(6).attr("title","Ændring: "+percent(res.bronze-old.bronze, true))
        tds.eq(7).attr("title","Ændring: "+percent(res.pos_4_6-old.pos_4_6, true))
      }
    } else if (simPart=="nedrykning") {
      tds.eq(4).html(percent(res.playoff))
      tds.eq(5).html(percent(res.pos_8_10))
      tds.eq(6).html(percent(res.nedrykning))
      console.log(Teams[t].name+": "+res.nedrykning)
      if (old) {
        tds.eq(4).attr("title","Ændring: "+percent(res.playoff-old.playoff, true))
        tds.eq(5).attr("title","Ændring: "+percent(res.pos_8_10-old.pos_8_10, true))
        tds.eq(6).attr("title","Ændring: "+percent(res.nedrykning-old.nedrykning, true))
      }
    } else {
      console.log("Noooo")
    }
  }
}

function arraysum(array) {
  return(array.reduce((a,b)=>a+b))
}

function sortTable(a,b) {
  return( (b.p*100+b.d) - (a.p*100+a.d) - 0.05 + Math.random()/10)
}

function getResult(res) {
  var dice=Math.random();
  // Home win distribution from all group matches after 2000
  if (res=="1") {
    if      (dice<0.2419) return([1,0]);
    else if (dice<0.4219) return([2,0]);
    else if (dice<0.6116) return([2,1]);
    else if (dice<0.7109) return([3,0]);
    else if (dice<0.8124) return([3,1]);
    else if (dice<0.8629) return([3,2]);
    else if (dice<0.9062) return([4,0]);
    else if (dice<0.9351) return([4,1]);
    else if (dice<0.9537) return([4,2]);
    else if (dice<0.9601) return([4,3]);
    else if (dice<0.9767) return([5,0]);
    else if (dice<0.9949) return([5,1]);
    else                  return([5,2]);
  }
  else if (res=="x") {
    if      (dice<0.3021) return([0,0]);
    else if (dice<0.7812) return([1,1]);
    else if (dice<0.9674) return([2,2]);
    else                  return([3,3]);
  }
  else if (res=="2") {
    if      (dice<0.2419) return([0,1]);
    else if (dice<0.4219) return([0,2]);
    else if (dice<0.5212) return([0,3]);
    else if (dice<0.5645) return([0,4]);
    else if (dice<0.5811) return([0,5]);
    else if (dice<0.7708) return([1,2]);
    else if (dice<0.8723) return([1,3]);
    else if (dice<0.9012) return([1,4]);
    else if (dice<0.9194) return([1,5]);
    else if (dice<0.9699) return([2,3]);
    else if (dice<0.9885) return([2,4]);
    else if (dice<0.9936) return([2,5]);
    else                  return([3,4]);
  }
  else alert("WRONG!");
}

function num1000(x) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}

function dumptable(table) {
  for (x in table)
    console.log("Team "+x+": "+table[x].g+" "+table[x].p+" "+table[x].d)
}

function dumpMatch(match, res=null) {
  var t1=Teams[match.hjemme]
  var t2=Teams[match.ude]
  debug(t1.short+" - "+t2.short+" "+dumpOdds(match.runodds)+": "+res[0]+"-"+res[1])
}

function dumpOdds(odds) {
  var p1=Math.round(odds[0]*100)
  var px=Math.round(odds[1]*100)
  var p2=Math.round(odds[2]*100)
  return("("+p1+","+px+","+p2+")")
}

function help_skill() {
  help("Holdenes styrke",
       "<p>Her kan du med skyderne angive styrken af de enkelte hold. Det er vores "+
       "eget mål for styrke, så den absolutte værdi kan ikke rigtig tolkes. Det "+
       "der betyder noget, det er forskellen i styrke, når 2 hold mødes. På "+
       "baggrund af styrken beregner vi nogle odds/sandsynligheder på kampen. "+
       "Hvis du vil se hvad styrken betyder, så kan du se sandsynlighederne på "+
       "kampene længere nede ændrer sig, når du justerer holdenes styrke."+
       "<p>På de 2 små ikoner kan du nulstille styrken til vores forudberegnede "+
       "styrke eller give alle hold den samme styrke."+
       "<p>Styrken er fra start beregnet ud "+
       "fra hvor mange point holdene har fået frem til seneste afsluttede runde "+
       "PT runde "+Object.values(Teams)[0].g+
       ". Så superligaens nummer 1 på det tidspunkt "+
       "har pr. definition den højeste styrke.")
}

function help_result() {
  var helptext=""
  if (simPart=="slutspil") 
    helptext="komme i slutspillet"
  else if (simPart=="medaljer")
    helptext="vinde guld/sølv/bronce"
  else if (simPart=="nedrykning")
    helptext="rykke ned"
  else console.log("Fuck. Something is wrong")
  help("Resultat af simuleringerne",
       "<p>I dette skema kan du se resultatet af simuleringerne. Vi simulerer alle "+
       "kampene "+num1000(nSimulations)+" gange og skemaet viser så hvor mange "+
       "point hvert hold i gennemsnit ender med samt hvilken sandsynlighed holdene "+
       "har for at "+helptext+
       "<p>Hvis du kører flere simulationer, så kan du holde musen over et resultat "+
       "for at se forskellen i chance mellem de 2 seneste "+
       "simuleringer. På den måde, så kan man f.eks. sætte resultatet i en enkelt "+
       "kamp og køre en ny simulation, og se hvor stor en betydning det vil have "+
       "for holdenes muligheder.")
}

function help_run() { 
  help("Kør simulation",
       "<p>Her er start-knappen, der starter en ny simulering. Når du klikker \"kør\" "+
       "så bliver alle kampe spillet igennem "+num1000(nSimulations)+" gange. Man "+
       "kan se hvor lang tid det tager i progress-baren. Hver gang du starter en "+
       "ny simulering, så starter den forfra, og spiller alle kampene igen.")
}

function help_match() { 
  help("Kamp-oversigt",
       "<p>Her kan du se alle de resterende relevante kampe i sæsonen. "+
       "De øverste kampe er måske afviklet, og har som "+
       "udgangspunkt resultatet af kampen angivet. For de endnu ikke afviklede kampe "+
       "er der 3 input felter, som angiver en relativ sandsynlighed for hhv. hjemmesejr, "+
       "uafgjort og udesejr. Hvis et tal f.eks.er dobbelt så stort som et andet, så "+
       "angiver det, at sandsynligheden for det udfald er dobbel så stor. Så tallene "+
       "behøver altså ikke summe til 100 eller et andet tal. 1-1-1 er det samme som "+
       "20-20-20 og begge angiver 33.3% chance for hvert udfald. Hvis du holder musen "+
       "over sandsynlighederne, så kommer der en lille pop-up besked, som viser hvad "+
       "sandsynlighederne svarer til i de odds, man finder hos bookmakerne."+
       "<p>Du kan frit ændre på sandsynlighederne, men vær opmærksom på, at hvis du "+
       "efterfølgende ændrer på et holds styrke i toppen, så vil sandsynlighederne "+
       "ændre sig, for de kampe det hold er med i og dine manuelt indtaste tal bliver "+
       "overskrvet. '1x2' feltet er en genvej til at "+
       "sætte sandsynligheden for 1,x eller 2 til 100%. Klik flere gange for at "+
       "skifte mellem et sikkert 1,x eller 2-tal."+
       "<p>Man kan også angive et specifikt resultat (antal mål) i stedet for "+
       "sandsynligheder. Hvis man klikker på 'odds/res', så kan man skifte mellem "+
       "sandsynligheder og kamp-resultat. Dvs. når du vælger 'resultat' så vil "+
       "de 3 felter med sandsynligheder blive erstattet af 2 felter, hvor man kan "+
       "angive antallet af mål.<p>Den sidste reset-knap kan bruges til at "+
       "nulstille sandsynligheden for kampen tilbage til start-værdierne.")
}

function help(title, text) {
  $("#helpdialog").html(text);
  $("#helpdialog").dialog({
    title:title
  });
}

function drawChart(team) {
  var res=Teams[team].results.position;
  var list=new Array();
  var p;
  list.push(["Position", "Sandsynlighed"]);
  res.forEach(function(x,i) {
    list.push([i+1, +(100*x).toFixed(2)]);
  })

  var data = google.visualization.arrayToDataTable(list);

  var options = {
    legend: { position: 'none' },
    backgroundColor: '#F5F5FF',
    title: 'Chancefordeling for '+team,
    hAxis: {minValue:1, maxValue:12, gridlines:{count:12}, textStyle: {color:'#005500', fontSize: 9} }
  };

  var chart = new google.visualization.ColumnChart(document.getElementById('chartdiv'));
  chart.draw(data, options);
}

