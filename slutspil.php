<?php 

require_once($_SERVER["DOCUMENT_ROOT"]."/genbrug/helper.php");
require($_SERVER["DOCUMENT_ROOT"]."/genbrug/db.txt");

$page_meta = "Hvem kommer i slutspillet? Du har nu chancen for at lege spåkone. Du indtaster dine gæt på de resterende kampe. Du spiller sæsonen et antal gange. Vupti, vi spytter sandsynlighederne ud for hvem der bliver mester, og rykker ned. Vi afliver spændingen!";
$page_title = "Hvem kommer i slutspillet?  - SuperStats";


$season=current_season();

require("top.php"); 

function sprintfu8 ($format) {
  $args = func_get_args();
  for ($i = 1; $i < count($args); $i++)
    $args [$i] = iconv('UTF-8', 'ISO-8859-1//TRANSLIT', $args [$i]);
  return(iconv('ISO-8859-1', 'UTF-8', call_user_func_array('sprintf', $args)));
}

function printfu8($format) {
  $args=func_get_args();
  print(call_user_func_array('sprintfu8',$args));
}

$dbh = Database::get_handle();

$round=$dbh->get_single_value("select coalesce(min(runde),23) from superstats.pgram where kampid not in (select kampid from superstats.res)");
 
if ($round>22) {
  print("
		<div class='box full blue multipleheader compactimg'>
			<h2 class='bighead'> Hvem kommer i slutspillet? </h2>
			<table> 
        <thead> <tr> <th class='headhelp'><div id='space20'></div> Grundspillet er afgjort... Så det bliver lidt kedeligt</thead>
        <tbody> <tr> <td> <p> Måske vi i stedet kan friste med 
          <p> <a href='medaljer.php'>Hvem der vinder medaljer</a> 
          <p> eller 
          <p> <a href='nedrykning.php'>Hvem der rykker ned</a><div id='space20'></div>
      </table>
		</div>\n");
	require($_SERVER["DOCUMENT_ROOT"]."/genbrug/bund.txt"); 
  die();
}

$tablesql = "select a.tid,a.target as short,c.klub as name,a.pos,".
            "   a.g, a.p, a.gf, a.ga,".                           // Current table
            "   b.g as g0,b.p as p0,b.gf as gf0, b.ga as ga0,".   // Table before list of matches played (typically 1/2 rounds before current table)
            "   a.g as gs, a.p as ps ".                           // Table games/points used for automatic strengt calculations
            "from superliga.tables_current as a ".
            "join superliga.tables_all as b on a.season=b.season and a.target=b.target and b.round=$round-2 ".
            "join superstats.team as c on holdid=a.tid and a.season=func.current_season() order by pos";

#$tablesql = "select tid,target as short,klub as name,pos,g,p,g as g0,p as p0,gf,ga from superliga.tables_all as t join superstats.team on holdid=tid and season=func.current_season() and round=$tround order by pos";
$tableres = $dbh->kquery($tablesql);

$matchquery="select p.kampid,runde,date_format(dato, '%d/%m %H:%i') as dato,hjemme,ude,hjemmemaal,udemaal from superstats.pgram as p ".
            "left join superstats.res as r on p.kampid=r.kampid ".
            "where func.aar2season(aar)=func.current_season() and runde>=$round-1 and runde<=22 order by runde,dato";
$matchres = $dbh->kquery($matchquery);
$matchlist=array();
while ($m = $matchres->fetch_assoc()) {
  $id=$m["runde"];
  if (!array_key_exists($id,$matchlist)) $matchlist[$id]=array();
  $matchlist[$id][]=$m;
}
$table=$tableres->fetch_all(MYSQLI_ASSOC);
$matchres->data_seek(0);
$tableres->data_seek(0);

function idarray($result, $idcol) {
  $output=array();
  while ($e = $result->fetch_assoc()) $output[$e[$idcol]]=$e;
  return($output);
}

# Make some Javascript variables the quick way
$js_teams=json_encode(idarray($tableres, "tid"),    JSON_NUMERIC_CHECK);
$js_match=json_encode(idarray($matchres, "kampid"), JSON_NUMERIC_CHECK);

if (array_key_exists("nsim",$_GET)) $nsim=$_GET["nsim"]; else $nsim=50000;

print("<script type=\"application/javascript\">\n");
print("  simPart='slutspil'\n");
print("  nSimulations=$nsim\n");
print("  Teams=$js_teams\n");
print("  Matches=$js_match\n");
print("</script>\n");

?>

<div class='box full blue multipleheader compactimg'>
  <h2 class='bighead'> Hvem kommer i slutspillet? </h2>
  <table> <thead> <tr> <th class='headhelp'> Se hjælp nederst på siden - eller klik på spørgsmålstegnene... </thead> </table>
</div>

<div class='box full blue multipleheader compactimg'>
  <h2> Aktuel stilling og holdenes formodede styrke</h2>
    <table cellpadding='0' cellspacing='0' id='table_current' class='table_standard'>
      <thead>
        <tr>
          <th> Pos </th>
          <th> </th>
          <th class='leftalign'><span>Klub</span></th>
          <th> Kampe   </th>
          <th> Point   </th>
          <th> Mål     </th>
          <th> Styrke <span> &nbsp; &nbsp; <img src='img/reset.svg' width="14" id="str_reset" title="Nulstil til start-værdier"> </span> <span><img src='img/equal.png' width="14" id="str_equal" title="Gør alle hold lige gode"></span> &nbsp; <span class='help'>(?)</span></th>
        </tr>
      </thead>
      <tfoot>
        <tr><td colspan='100%'>&nbsp;</td></tr>
      </tfoot>
      <tbody>
<?php
  $str = "<input type='range' min='0' max='15' step='.1' value='5' data-tid='%d'> <span class='skillspan'> 0.0 </span>";
  foreach ($table as $e) {
    $tid=$e["tid"];
    $team=SLTeams::team($tid);
    printf("        <tr id=%-8s> <td> %2d <td> %s <td> %s <td> %d <td> %d <td> %d - %d <td> $str\n",
          "'pos_$tid'",$e["pos"],$team->logolink($season), $team->fulllink($season), $e["g"], $e["p"], $e["gf"], $e["ga"], $tid);
  }
?>
      </tbody>
    </table>
</div>

<div class='box full blue multipleheader compactimg'>
  <h2> Simuleret chance for slutspil </h2>

    <table cellpadding='0' cellspacing='0' id='table_result' class='table_standard'>
      <thead>
        <tr>
          <th> Pos </th>
          <th> </th>
          <th class='leftalign'><span>Klub</span></th>
          <th> Gns. point     </th>
          <th> Gns. placering </th>
          <th> Slutspil %    </th>
          <th> Ændring       </th>
          <th> <span id='help_result' class='help'> (?) </span> </th>
        </tr>
      </thead>
      <tfoot>
        <tr><td colspan='100%'>&nbsp;</td></tr>
        <tr><td colspan='3'> <button> Kør ny simulation </button> <td colspan='4'> <div id='progressbar'><div id='progresslabel'>Fuldført: 0%</div></div><td> <span id='help_run' class='help'> (?) </span>
      </tfoot>
      <tbody>
<?php
  foreach ($table as $e) {
    $tid=$e["tid"];
    $team=SLTeams::team($tid);
    printf("        <tr id='res_$tid'> <td> {$e["pos"]} <td> %s <td> %s <td> 0.0 <td> 0.0 <td> 0.0%% <td> 0.0%% <td>\n",
          $team->logolink($season), $team->fulllink($season)); 
  }
?>
      </tbody>
    </table>
</div>
<div id='helpdialog'></div>

<?php
function roundheader($round) {
echo "  <table cellpadding='0' cellspacing='0' id='table_matchlist'>
    <thead>
      <tr> <th class='leftalign' colspan='2'>Runde $round</th> <th colspan='100%'><span class='help'>(?)</span></tr>
    </thead>
";
}
function roundmatch($m) {
  $home=SLTeams::team($m["hjemme"]);
  $away=SLTeams::team($m["ude"]);
  printfu8("      <tr id='%s'> <td> %s <td> %-15s <td> - <td> %-15s <td> <span class='spanodds'> <input> <input> <input> </span> <span class='spanresult'> <input value='{$m["hjemmemaal"]}'> - <input value='{$m["udemaal"]}'> </span> <td class='outcome'> <span title='S&aelig;t resultat til 1,x eller 2'>1x2</span> <td class='restype' title='Skift mellem sandsynligheder og kampresultat'> <span class='typeselected'>odds</span>/<span class='typeunselected'>res</span> <td> <span class='oddsreset' title='Nulstil til automatisk beregnede sandsynligheder'> reset </span>\n",
    "match_".$m["kampid"], $m["dato"], $home->name, $away->name);
}


print("<div class='box full blue multipleheader compactimg'>\n".
      "  <h2>Resterende grundspilskampe</h2>\n");

foreach($matchlist as $round=>$data) {
  roundheader($round);
  print("    <tbody>\n");
  foreach ($data as $d) {
    roundmatch($d);
  }
  print("    </tbody>\n".
        "  </table>\n");
}
?>
</div>

<div class='box full blue multipleheader'>
  <h2>Hvordan virker det pjat her?</h2>
  <div id='explain'>
    <p>Her kan du lege lidt med de enkelte holds sandsynlighed for at komme i
    slutspillet - dvs. ende i top-6 efter 22 runder. Det er en klassisk Monte
    Carlo simulering, hvor man opstiller nogle sandsynligheder for nogle
    udfald, og så lader en computer afgøre tilfældighederne et stort antal
    gange, hvorefter man tror på, at det gennemsnit man finder, forhåbentlig
    afspejler virkeligheden på fornuftig vis.

    <p>Klik på de små spørgsmålstegn for at få en forklaring på hvad de enkelte
    elementer betyder.

    <p>For at køre en simulation skal man sætte sandsynligheder på udfaldene
    af de resterende kampe i grundspillet. Dvs. man kan frit indtaste
    sandsynligheder i de 3 input-felter for hver kamp. De 3 felter bliver
    tolket som "relative" sandsynligheder, så det er kun størrelsesforholdet
    mellem de 3 tal, der betyder noget - ikke den absolutte værdi.  1-1-1 er
    det samme som 5-5-5 og 33-33-33. Og 1-0-0 er det samme som 10-0-0 og de 2
    sidste betyder begge en helt sikker hjemmesejr. Hvis man lader musen stå
    henover nogle odds, så vil en lille pop-up besked vise hvilke odds
    sandsynlighederne svarer til.
    
    <p>Man kan også vælge at indtaste et specifikt resultat i stedet for
    sandsynligheder for en eller flere af kampene. Bare klik på "odds/res" for
    at skifte mellem sandsynligheder og faktisk resultat (antal mål til hvert
    hold).

    <p> Hvis man klikker på "1x2" så kan man skifte mellem en sikker hjemmesejr
    / uafgjort / udesejr og hvis man klikker på "reset", så sætter man
    sandsynlighederne tilbage til de automatisk beregnede.

    <p>Som udgangspunkt bliver sandsynlighederne sat ud fra den "styrke" man
    sætter på holdene i den øverste tabel. Jo højere styrke man sætter på et
    hold, des større chance for at vinde kampene. Sandsynlighederne ændres
    automatisk, når man ændrer holdenes styrke. Øverst kan man ved at klikke på
    de små ikoner sætte styrken tilbage til det oprindelige eller sætte styrken
    ens for alle hold. Start-styrken er sat til den styrke, som svarer til
    det antal point holdene fik i løbet af efteråret.
    
    <p>Sandsynlighederne udregnes efter en simpel model for en
    fodboldkamp. Man kan se koden i <a
    href='/js/MatchProbability.js'>Javascript-filen</a>, hvis man har lyst til
    det.

  </div>
  <div id='chartdiv'></div> <!-- Not active yet -->
</div>


<?php
require($_SERVER["DOCUMENT_ROOT"]."/genbrug/bund.txt"); 

?>
